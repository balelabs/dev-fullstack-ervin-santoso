import axios from 'axios';
import { API_URL, GET_CUSTOMER } from '../../utils/constants';

export const fetchCustomer = () => {
    return (dispatch) => {
        axios
        .get(API_URL + "customer")
        .then(function (response) {
            setTimeout(() => { 
                dispatch({
                    type: GET_CUSTOMER,
                    payload:  {
                        loading: false,
                        data:  response.data.data,
                        errorMessage: false,
                    },
                });
            },200)
        })
        .catch(function (error) {
            setTimeout(() => { 
                dispatch({
                    type: GET_CUSTOMER,
                    payload: {
                        loading: false,
                        data: false,
                        errorMessage: error.message,
                    },
                });
            },200)
        });
    };
};