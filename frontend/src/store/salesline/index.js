import axios from 'axios';
import { API_URL, GET_SALESLINE } from '../../utils/constants';

export const fetchSalesLine = () => {
    return (dispatch) => {
        axios
        .get(API_URL + "salesline")
        .then(function (response) {
            setTimeout(() => { 
                dispatch({
                    type: GET_SALESLINE,
                    payload:  {
                        loading: false,
                        data:  response.data.data,
                        errorMessage: false,
                    },
                });
            },200)
        })
        .catch(function (error) {
            setTimeout(() => { 
                dispatch({
                    type: GET_SALESLINE,
                    payload: {
                        loading: false,
                        data: false,
                        errorMessage: error.message,
                    },
                });
            },200)
        });
    };
};