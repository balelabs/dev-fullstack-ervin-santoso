import React, { Component } from "react";
import { Route, Switch } from "react-router-dom";

import HeaderComponent from "./components/HeaderComponent";

import HomeComponent from "./components/HomeComponent";
import PackageComponent from "./components/PackageComponent";
import CustomerComponent from "./components/CustomerComponent";
import SalesComponent from "./components/SalesComponent";
import AddSales from "./pages/sales/AddSales";
import EditSales from "./pages/sales/EditSales";

export default class App extends Component {
    render() {
        return (
            <>
                <HeaderComponent />
                <Switch>
                    <Route path="/" exact component={HomeComponent} />
                    <Route path="/package" exact component={PackageComponent} />
                    <Route path="/customer" exact component={CustomerComponent} />
                    <Route path="/sales" exact component={SalesComponent} />
                    <Route path="/add" exact component={AddSales} />
                    <Route path="/sales/:id" exact component={EditSales} />
                </Switch>
            </>
        );
    }
}
