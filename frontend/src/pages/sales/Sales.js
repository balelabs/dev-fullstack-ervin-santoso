import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { Col, Container,Row } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { fetchSales } from '../../store/sales';
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';

const Sales = () => {
    const { itemsSales}                     = useSelector(state => state.salesReducer);
    const[deleteSalesData,setDeleteSales]   = useState(false);
    const dispatch                          = useDispatch();

    useEffect(() => {
        dispatch(fetchSales());
    },[dispatch]);

    useEffect(() => {
        if(deleteSalesData){
            setDeleteSales(true);
            dispatch(fetchSales()); // eslint-disable-next-line
        }
    },[dispatch]); // eslint-disable-next-line
    
    const deleteSales = async (id) =>{
        await axios.delete(`http://127.0.0.1:8000/api/sales/${id}`);
        fetchSales();
    }

    const columns = [{
        dataField: 'id',
        text: 'No',
        headerStyle: () => {
            return { width:"5%" };
        },
      }, {
        dataField: 'customer_id',
        text: 'Customer',
        sort: true,
        headerStyle: () => {
            return { width:"10%" };
        },
      }, {
        dataField: 'inv_number',
        text: 'Invoice',
        sort: true,
        headerStyle: () => {
            return { width:"10%" };
        },
      },
      {
        dataField: 'package_id',
        text: 'Package',
        sort: true,
        headerStyle: () => {
            return { width:"10%" };
        },
      },
      {
        dataField: 'inv_date',
        text: 'Date',
        sort: true,
        headerStyle: () => {
            return { width:"10%" };
        },
      }, {
        dataField: 'amount',
        text: 'Amount',
        sort: true,
        headerStyle: () => {
            return { width:"10%" };
        },
      },{
        dataField: 'total_discount_amount',
        text: 'Discount',
        sort: true,
        headerStyle: () => {
            return { width:"10%" };
        },
      }, {
        dataField: 'total_amount',
        text: 'Total Amount',
        sort: true,
        headerStyle: () => {
            return { width:"10%" };
        },
      },
      {
        dataField: 'link',
        text: 'Action',
        headerStyle: () => {
            return { width:"15%" };
        },
        formatter: (rowContent,row) => {
            return(
                <div>
                    <Link to={`/sales/${row.id}`} className="btn btn-info btn-sm mr-2">Edit</Link>
                    <button onClick={() => deleteSales(row.id)} className="btn btn-danger btn-sm">Delete</button>
                </div>
            )
        }
      }];

      const defaultSorted = [{
        dataField: 'id',
        order: 'desc'
      }];
      const customTotal = (from, to, size) => (
        <span className="react-bootstrap-table-pagination-total">
          Showing { from } to { to } of { size } Results
        </span>
      );
      const options = {
        paginationSize: 4,
        pageStartIndex: 0,
        // alwaysShowAllBtns: true, // Always show next and previous button
        // withFirstAndLast: false, // Hide the going to First and Last page button
        // hideSizePerPage: true, // Hide the sizePerPage dropdown always
        // hidePageListOnlyOnePage: true, // Hide the pagination list when only one page
        firstPageText: 'First',
        prePageText: 'Back',
        nextPageText: 'Next',
        lastPageText: 'Last',
        nextPageTitle: 'First page',
        prePageTitle: 'Pre page',
        firstPageTitle: 'Next page',
        lastPageTitle: 'Last page',
        showTotal: true,
        paginationTotalRenderer: customTotal,
        disablePageTitle: true,
        sizePerPageList: [{
          text: '5', value: 5
        }, {
          text: '10', value: 10
        }, {
          text: 'All', value: itemsSales.length
        }] // A numeric array is also available. the purpose of above example is custom the text
      };
    return (
        <>
            <section>
                <Container>
                    <Row>
                        <Col>
                            <Link to="/add" className="btn btn-primary mt-5">Add Data</Link>
                            <br />
                            <br />
                            <BootstrapTable bootstrap4 keyField='id' data={ itemsSales } columns={ columns } defaultSorted={ defaultSorted }  pagination={ paginationFactory(options) }/>
                        </Col>
                    </Row>
                </Container>
            </section>
        </>
    )
}

export default Sales