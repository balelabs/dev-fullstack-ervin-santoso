import { GET_PACKAGE } from "../../utils/constants";

const initialState = {
    itemsPackage: [],
    isFetching: false,
    error: false
}

const packageReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_PACKAGE:
            return {
                ...state,
                itemsPackage: action.payload.data,
                isFetching: true,
                error: action.payload.errorMessage,
            };

        default:
            return state;
    }
};
export default packageReducer