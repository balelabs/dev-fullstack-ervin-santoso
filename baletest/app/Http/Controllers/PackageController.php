<?php

namespace App\Http\Controllers;

use App\Models\Package;
use Illuminate\Http\Request;

class PackageController extends BaseController
{
   
    public function index()
    {
        $package           = Package::all();
        return $this->sendResponse($package, 'successfully.');
      
    }

}
