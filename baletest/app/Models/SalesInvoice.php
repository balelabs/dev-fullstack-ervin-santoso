<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SalesInvoice extends Model
{
    use HasFactory;
    protected $guarded  = [];

    public function customer(){
       return $this->belongsTo('App\Models\Customer');
    }

    public function package(){
       return $this->hasOne('App\Models\SalesInvoiceLine');
    }
}
